<table class="table table-striped">
  <thead>
    <tr>
      <td colspan="2" class="border">
        Un abonnement idéal pour un maximum de 5 personnes. Inclut des applications de productivité, un maximum de 5 To de stockage en ligne (1 To par personne) et des fonctionnalités de sécurité avancées pour tous vos appareils.<br><br>
        
          <strong >Logiciels et services inclus</strong> <br>
          <div class="row ">
            <div class="col">
                <span >
                    <img src="../assets/images/office/word.png" style="height: 35px;"><br>Word
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/excel.png" style="height: 35px;"><br>Excel
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/powerpoint.png" style="height: 35px;"><br>PowerPoint
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/onenote.png" style="height: 35px;"><br>OneNote
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/outlook.png" style="height: 35px;"><br>Outlook
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/access.png" style="height: 35px;"><br>Access
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/publisher.png" style="height: 35px;"><br>Publisher
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/onedrive.png" style="height: 35px;"><br>OneDrive
                </span>
            </div>
            <div class="col">
                <span >
                    <img src="../assets/images/office/skype.png" style="height: 35px;"><br>Skype
                </span>
            </div>
        </div>
        </p>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="width: 70%">
        <strong>Utilisation</strong>
      </td>
      <td class="border">
         Jusqu’à 5 personnes
      </td>
    </tr>
    <tr>
      <td class="border">
          <strong>Word, Excel, PowerPoint, OneNote</strong><br>
          Travaillez hors connexion et appliquez une mise en forme avancée avec de puissantes applications de bureau.
        </p>
      </td>
      <td class="border">
         Premium
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Stockage en ligne OneDrive</strong><br>
        Sauvegardez vos fichiers et photos, et accédez-y sur tous vos appareils. Enregistrez vos documents Office sur OneDrive pour activer l’enregistrement automatique et faciliter le partage de fichiers.
      </td>
      <td class="border" >
         Jusqu’à 5 To<br>(1 To par personne)
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Outlook</strong><br>
Rassemblez vos e-mails et calendriers dans une seule application de bureau.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Outils créatifs optimisés par l’intelligence artificielle</strong><br>
Bénéficiez de suggestions de conception et de rédaction utiles grâce à des fonctionnalités telles que Concepteur, Idées et le nouveau Rédacteur Microsoft.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Contenu créatif Premium</strong><br>
Accédez à une vaste collection de contenus créatifs libres de droits, comprenant des modèles, des images de photothèque, des icônes et des polices.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Sécurité avancée de OneDrive</strong><br>
Sachez que vos fichiers sont protégés par des fonctionnalités intégrées de détection des rançongiciels et de récupération et qu’une vérification d’identité en deux étapes est nécessaire pour accéder aux fichiers les plus importants stockés dans votre Coffre-fort OneDrive.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Skype</strong><br>
Restez en contact avec vos amis et votre famille sur leurs téléphones mobiles ou fixes avec 60 minutes d’appels Skype par mois.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Fonctionne sur Windows, macOS, iOS et Android4</strong><br>
Utilisez-le sur vos appareils préférés à la maison ou en déplacement.
      </td>
      <td class="border" >
         <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
      </td>
    </tr>
    <tr>
      <td class="border">
        <strong>Mode d'achat</strong><br>

      </td>
      <td class="border" >
         Achat une seule fois pour toujours
      </td>
    </tr>
  </tbody>
</table>
