<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.12.4, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="../assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" type="image/x-icon">
  <meta name="description" content="Microsoft Office 365 s’appelle désormais Microsoft 365. Un nouveau nom et plus d’avantages pour le même prix">
  
  
  <title>Microsoft Office 365</title>
  <link rel="stylesheet" href="../assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="../assets/tether/tether.min.css">
  <link rel="stylesheet" href="../assets/dropdown/css/style.css">
  <link rel="stylesheet" href="../assets/socicon/css/styles.css">
  <link rel="stylesheet" href="../assets/theme/css/style.css">
  <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="menu cid-s7UX4kTSAd" once="menu" id="menu1-s">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="../">
                         <img src="../assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-white display-4" href="../">
                        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="../aide.php"><span class="mbri-info mbr-iconfont mbr-iconfont-btn"></span>Aide</a>
                </li><li class="nav-item"><a class="nav-link link text-white display-4" href="../contact.php"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span>
                        
                        Contact</a></li></ul><!-- 
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="https://mobirise.co"><span class="mbri-arrow-down mbr-iconfont mbr-iconfont-btn"></span>
                    
                    Voir les offres</a></div> -->
        </div>
    </nav>
</section>
<section class="features8 cid-s7VIeG8JtE" id="features8-16">

    

    <div class="container">
      <form>
        <div class="form-group">
          <label for="exampleFormControlSelect1">Votre produit :</label>
          <select class="form-control" id="exampleFormControlSelect1">
            <option>Microsoft 365 Famille - 35€</option>
            <option>Microsoft 365 Personnel - 25€</option>
          </select>
        </div>
      </form>
        <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2"><img src="../assets/images/microsoft_office_logo.png" style="height: 40px;"> Microsoft 365 Famille</h2>

    </div>

    <div class="container">
        <div class="media-container-row">
          <div class="col-12 col-md-12">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td colspan="2" class="border">
                    Un abonnement idéal pour un maximum de 5 personnes. Inclut des applications de productivité, un maximum de 5 To de stockage en ligne (1 To par personne) et des fonctionnalités de sécurité avancées pour tous vos appareils.<br><br>
                    
                      <strong >Logiciels et services inclus</strong> <br>
                      <div class="row ">
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/word.png" style="height: 35px;"><br>Word
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/excel.png" style="height: 35px;"><br>Excel
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/powerpoint.png" style="height: 35px;"><br>PowerPoint
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/onenote.png" style="height: 35px;"><br>OneNote
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/outlook.png" style="height: 35px;"><br>Outlook
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/access.png" style="height: 35px;"><br>Access
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/publisher.png" style="height: 35px;"><br>Publisher
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/onedrive.png" style="height: 35px;"><br>OneDrive
                            </span>
                        </div>
                        <div class="col">
                            <span >
                                <img src="../assets/images/office/skype.png" style="height: 35px;"><br>Skype
                            </span>
                        </div>
                    </div>
                    </p>
                  </td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td style="width: 70%">
                    <strong>Utilisation</strong>
                  </td>
                  <td class="border">
                     Jusqu’à 5 personnes
                  </td>
                </tr>
                <tr>
                  <td class="border">
                      <strong>Word, Excel, PowerPoint, OneNote</strong><br>
                      Travaillez hors connexion et appliquez une mise en forme avancée avec de puissantes applications de bureau.
                    </p>
                  </td>
                  <td class="border">
                     Premium
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Stockage en ligne OneDrive</strong><br>
                    Sauvegardez vos fichiers et photos, et accédez-y sur tous vos appareils. Enregistrez vos documents Office sur OneDrive pour activer l’enregistrement automatique et faciliter le partage de fichiers.
                  </td>
                  <td class="border" >
                     Jusqu’à 5 To<br>(1 To par personne)
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Outlook</strong><br>
Rassemblez vos e-mails et calendriers dans une seule application de bureau.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Outils créatifs optimisés par l’intelligence artificielle</strong><br>
Bénéficiez de suggestions de conception et de rédaction utiles grâce à des fonctionnalités telles que Concepteur, Idées et le nouveau Rédacteur Microsoft.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Contenu créatif Premium</strong><br>
Accédez à une vaste collection de contenus créatifs libres de droits, comprenant des modèles, des images de photothèque, des icônes et des polices.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Sécurité avancée de OneDrive</strong><br>
Sachez que vos fichiers sont protégés par des fonctionnalités intégrées de détection des rançongiciels et de récupération et qu’une vérification d’identité en deux étapes est nécessaire pour accéder aux fichiers les plus importants stockés dans votre Coffre-fort OneDrive.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Skype</strong><br>
Restez en contact avec vos amis et votre famille sur leurs téléphones mobiles ou fixes avec 60 minutes d’appels Skype par mois.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Fonctionne sur Windows, macOS, iOS et Android4</strong><br>
Utilisez-le sur vos appareils préférés à la maison ou en déplacement.
                  </td>
                  <td class="border" >
                     <span class="mbri-success display-5 mbr-iconfont mbr-iconfont-btn"></span>
                  </td>
                </tr>
                <tr>
                  <td class="border">
                    <strong>Mode d'achat</strong><br>

                  </td>
                  <td class="border" >
                     Achat une seule fois pour toujours
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row justify-content-md-center justify-content-center">
          <div class="col-md-6 offset-md-3">

            <form>
              <button class="btn btn-secondary">Continuer&nbsp;<span class="mbri-right display-5 mbr-iconfont mbr-iconfont-btn"></span></button>
            </form>
          </div>
        </div>
    </div>
</section>
<section class="cid-s7xAamyasU mbr-reveal" id="footer1-d">

    

    

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="https://mobirise.co/">
                        <img src="../assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email : moussainssa@outlook.fr &nbsp;&nbsp;<br>Tél : +33 (0) 7 68 32 36 99 &nbsp;</p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">&nbsp;
                </h5>
                <p class="mbr-text">
                    <a href="../contact.php" class="btn btn-secondary">M'écrire</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Business Software - Tous droits réservés</p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-snapchat socicon" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                            </a>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <script src="../assets/web/assets/jquery/jquery.min.js"></script>
  <script src="../assets/popper/popper.min.js"></script>
  <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../assets/tether/tether.min.js"></script>
  <script src="../assets/web/assets/cookies-alert-plugin/cookies-alert-core.js"></script>
  <script src="../assets/web/assets/cookies-alert-plugin/cookies-alert-script.js"></script>
  <script src="../assets/smoothscroll/smooth-scroll.js"></script>
  <script src="../assets/dropdown/js/nav-dropdown.js"></script>
  <script src="../assets/dropdown/js/navbar-dropdown.js"></script>
  <script src="../assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="../assets/theme/js/script.js"></script>
  
  
<input name="cookieData" type="hidden" data-cookie-customDialogSelector='null' data-cookie-colorText='#424a4d' data-cookie-colorBg='rgba(255, 127, 159, 0.99)' data-cookie-textButton='Ok' data-cookie-colorButton='' data-cookie-colorLink='#424a4d' data-cookie-underlineLink='true' data-cookie-text="En poursuivant votre navigation sur notre site, vous acceptez l'installation et l'utilisation de cookies sur votre poste. Lire notre <a href='#'>politique de confidentialité</a>.">
  </body>
</html>