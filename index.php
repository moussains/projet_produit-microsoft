<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.12.4, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" type="image/x-icon">
  <meta name="description" content="Business Software, meilleur site de promotion de logiciels (Microsoft Office 365, Antivirus, Système d'exploitation Windows 10) ">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  
  <title>Logiciel moins cher - Business Software</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="menu cid-s7V4kHr8CA" once="menu" id="menu1-13">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="./">
                         <img src="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-white display-4" href="./">
                        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="aide.php"><span class="mbri-info mbr-iconfont mbr-iconfont-btn"></span>Aide</a>
                </li><li class="nav-item"><a class="nav-link link text-white display-4" href="contact.php"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span>
                        
                        Contact</a></li></ul><!-- 
            <div class="navbar-buttons mbr-section-btn">
                <a class="btn btn-sm btn-primary display-4" href="#"><span class="mbri-arrow-down mbr-iconfont mbr-iconfont-btn"></span> Voir les offres</a>
            </div> -->


        </div>
    </nav>
</section>

<section class="header12 cid-s7xuog6YWD mbr-fullscreen mbr-parallax-background" id="header12-8">

    

    <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(35, 35, 35);">
    </div>

    <div class="container  ">
            <div class="media-container">
                <div class="col-md-12 align-center">
                    <h1 class="mbr-section-title pb-3 mbr-white mbr-bold mbr-fonts-style display-1">
                        BUSINESS SOFTWARE</h1>
                    <p class="mbr-text pb-3 mbr-white mbr-fonts-style display-5">
                        Meilleur Bon Plan Microsoft 365</p>
                    <div class="mbr-section-btn align-center py-2">
                        <a class="btn btn-md btn-primary display-4" href="#features8-16">
                            Découvrir les offres &nbsp;&nbsp;<img src="assets/images/microsoft_office_logo.png" style="height: 35px;">
                        </a>
                    </div>

                    <div class="icons-media-container mbr-white">
                        <div class="card col-6 col-md-4 col-lg-4">
                            <div class="icon-block">
                            
                                <span class="mbr-iconfont mbri-protect" style="color: rgb(0, 177, 23); fill: rgb(0, 177, 23);"></span>
                            
                            </div>
                            <h5 class="mbr-fonts-style">
                                Produits 100% originaux</h5>
                        </div>

                        <div class="card col-6 col-md-4 col-lg-4">
                            <div class="icon-block">
                                
                                    <span class="mbr-iconfont mbri-user" style="color: rgb(15, 118, 153); fill: rgb(15, 118, 153);"></span>
                                
                            </div>
                            <h5 class="mbr-fonts-style">
                                Vendeur confirmé</h5>
                        </div>

                        <div class="card col-6 col-md-4 col-lg-4">
                            <div class="icon-block">
                                
                                    <span class="mbr-iconfont mbri-rocket" style="color: rgb(204, 41, 82); fill: rgb(204, 41, 82);"></span>
                                
                            </div>
                            <h5 class="mbr-fonts-style">
                                Livraison instantanée par mail</h5>
                        </div>

                        
                    </div>
                </div>
            </div>
    </div>

    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>


<section class="features8 cid-s7VIeG8JtE" id="features8-16">

    

    <div class="container">
        <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2"><img src="assets/images/microsoft_office_logo.png" style="height: 40px;"> Microsoft 365</h2>
        <h3 class="mbr-section-subtitle  display-5 align-center mbr-light mbr-fonts-style mb-4">Trouvez l’offre répondant à vos besoins</h3>

    </div>

    <div class="container">
        <div class="media-container-row">
            <div class="card  col-12 col-md-6">
            
               <!--  <div class="card-img">
                    <span class="mbri-desktop mbr-iconfont"></span>
                </div> -->
                <div class="card-box align-center">
                    <span class="alert alert-danger">Le plus recommandé (24 vendus)</span>
                    <h4 class="card-title mbr-fonts-style display-5">Microsoft 365 Famille</h4>
                    <span class="text-center mbr-fonts-style"><s><span class="display-5">99</span> € par an</s> <span class="display-2">35</span> € pour toujours</span>
                    <p class="mbr-text mbr-fonts-style display-7">
                       Une offre idéale pour un maximum de 5 personnes. Inclut des applications de productivité, un maximum de 5 To de stockage en ligne (1 To par personne) et des fonctionnalités de sécurité avancées pour tous vos appareils.
                    </p>
                    <div class="mbr-section-btn text-center"><a href="office/?famille" class="btn btn-secondary display-4">
                            Voir plus</a></div>
                </div>
            </div>

            <div class="card  col-12 col-md-6">
                <!-- <div class="card-img">
                    <span class="mbri-touch mbr-iconfont"></span>
                </div> -->
                <div class="card-box align-center">
                    <h4 class="card-title mbr-fonts-style display-5">Microsoft 365 Personnel</h4>
                    <span class="text-center mbr-fonts-style"><s><span class="display-5">69</span> € par an</s> <span class="display-2">25</span> € pour toujours</span>
                    <p class="mbr-text mbr-fonts-style display-7">
                       Une offre idéale pour 1 personne uniquement incluant des applications de productivité, 1 To de stockage dans le cloud et des fonctionnalités de sécurité avancées pour tous vos appareils.
                    </p>
                    <div class="mbr-section-btn text-center"><a href="office/?personnel" class="btn btn-secondary display-4">
                            Voir plus</a></div>
                </div>
            </div>
            

            

            
        </div>
        <div class="row ">
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/word.png" style="height: 35px;"><br>Word
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/excel.png" style="height: 35px;"><br>Excel
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/powerpoint.png" style="height: 35px;"><br>PowerPoint
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/onenote.png" style="height: 35px;"><br>OneNote
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/outlook.png" style="height: 35px;"><br>Outlook
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/access.png" style="height: 35px;"><br>Access
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/publisher.png" style="height: 35px;"><br>Publisher
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/onedrive.png" style="height: 35px;"><br>OneDrive
                </p>
            </div>
            <div class="col">
                <p class="text-center">
                    <img src="assets/images/office/skype.png" style="height: 35px;"><br>Skype
                </p>
            </div>
        </div>
    </div>
</section>

<section class="features9 cid-s7FzABixJy" id="features9-j">

    
    <div class="container">
        <h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2"><span class="mbr-iconfont mbri-setting" style="font-size: 50px;"></span>&nbsp;&nbsp;Déroulement</h2>
        <h3 class="mbr-section-subtitle  display-5 align-center mbr-light mbr-fonts-style mb-4">Comment ça doit fonctionner entre nous ?</h3>

    </div>
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="media-container-row">
                    <div class="card-img pr-2 text-center">
                        <span class="mbr-iconfont mbri-smile-face" style="font-size: 50px;"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7 text-center">
                            Avoir de la courtoise et du respect</h4>
                        
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="media-container-row">
                    <div class="card-img pr-2 text-center">
                        <span class="mbr-iconfont mbri-add-submenu" style="font-size: 50px;"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7 text-center">
                            Faire le choix du produit</h4>
                        
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="media-container-row">
                    <div class="card-img pr-2 text-center">
                        <span class="mbr-iconfont mbri-paper-plane" style="font-size: 50px;"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7 text-center">
                            M'envoyer votre adresse e-mail</h4>
                        
                    </div>
                </div>
            </div>

            <div class="card p-3 col-12 col-md-6 col-lg-3">
                <div class="media-container-row">
                    <div class="card-img pr-2 text-center">
                        <span class="mbr-iconfont mbri-cash" style="font-size: 50px;"></span>
                    </div>
                    <div class="card-box">
                        <h4 class="card-title py-3 mbr-fonts-style display-7 text-center">
                            Payer en ligne</h4>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cid-s7xAamyasU mbr-reveal" id="footer1-d">

    

    

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="https://mobirise.co/">
                        <img src="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email : moussainssa@outlook.fr &nbsp;&nbsp;<br>Tél : +33 (0) 7 68 32 36 99 &nbsp;</p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">&nbsp;
                </h5>
                <p class="mbr-text">
                    <a href="contact.php" class="btn btn-secondary">M'écrire</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Business Software - Tous droits réservés</p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-snapchat socicon" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                            </a>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/web/assets/cookies-alert-plugin/cookies-alert-core.js"></script>
  <script src="assets/web/assets/cookies-alert-plugin/cookies-alert-script.js"></script>
  <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
  <script src="assets/parallax/jarallax.min.js"></script>
  <script src="assets/mbr-tabs/mbr-tabs.js"></script>
  <script src="assets/dropdown/js/nav-dropdown.js"></script>
  <script src="assets/dropdown/js/navbar-dropdown.js"></script>
  <script src="assets/theme/js/script.js"></script>
  
  
<input name="cookieData" type="hidden" data-cookie-customDialogSelector='null' data-cookie-colorText='#424a4d' data-cookie-colorBg='rgba(255, 127, 159, 0.99)' data-cookie-textButton='Ok' data-cookie-colorButton='' data-cookie-colorLink='#424a4d' data-cookie-underlineLink='true' data-cookie-text="En poursuivant votre navigation sur notre site, vous acceptez l'installation et l'utilisation de cookies sur votre poste. Lire notre <a href='privacy.php'>politique de confidentialité</a>.">
  </body>
</html>