<!DOCTYPE html>
<html  >
<head>
  <!-- Site made with Mobirise Website Builder v4.12.4, https://mobirise.com -->
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Mobirise v4.12.4, mobirise.com">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
  <link rel="shortcut icon" href="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" type="image/x-icon">
  <meta name="description" content="Retrouvez toutes les informations pour vos produits. Comment pouvons-nous vous aider ?">
  
  
  <title>Aides</title>
  <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="assets/tether/tether.min.css">
  <link rel="stylesheet" href="assets/socicon/css/styles.css">
  <link rel="stylesheet" href="assets/dropdown/css/style.css">
  <link rel="stylesheet" href="assets/theme/css/style.css">
  <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css"><link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
  
  
  
</head>
<body>
  <section class="menu cid-s7UXrQljXH" once="menu" id="menu1-v">

    

    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="./">
                         <img src="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="" style="height: 3.8rem;">
                    </a>
                </span>
                
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-white display-4" href="./">
                        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"></span>
                        Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="aide.php"><span class="mbri-info mbr-iconfont mbr-iconfont-btn"></span>Aide</a>
                </li><li class="nav-item"><a class="nav-link link text-white display-4" href="contact.php"><span class="mbri-letter mbr-iconfont mbr-iconfont-btn"></span>
                        
                        Contact</a></li></ul><!-- 
            <div class="navbar-buttons mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="https://mobirise.co"><span class="mbri-arrow-down mbr-iconfont mbr-iconfont-btn"></span>
                    
                    Voir les offres</a></div> -->
        </div>
    </nav>
</section>

<section class="toggle1 cid-s7VVV6e2cX" id="toggle1-19">

    

    
        <div class="container">
            <div class="media-container-row">
                <div class="col-12 col-md-8">
                    <div class="section-head text-center space30">
                       <h2 class="mbr-section-title pb-5 mbr-fonts-style display-2">
                            L'aide en ligne</h2>
                    </div>
                    <div class="clearfix"></div>
                    <div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse1_29" aria-expanded="false" aria-controls="collapse1">
                                    <h4 class="mbr-fonts-style display-5">
                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span> Comment avoir l'offre ?</h4>
                                </a>
                            </div>
                            <div id="collapse1_29" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body p-4">
                                    <ul>
                                        <li><strong>Choisir votre offre : </strong>Allez sur la page d'accueil du site, choisissez votre offre en cliquant sur le bouton "<strong>voir plus</strong>". Vous êtes maintenant dans la page de descrption de l'offre que vous avez choisi. Appuyez ensuite sur le bouton "<strong>continuer</strong>" . <a href="./">Essayer maintenant !</a></li>
                                        <li><strong>Saisir vos infos personelles :</strong> Après avoir choisi votre offre, vous serez redirigé automatiquement dans la page du récapitulatif de votre  commande, où vous pouvez saisir vos informations. <a href="./office/infos-perso.php">Essayer maintenant !</a></li>
                                        <li><strong>Payer en ligne :</strong></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo">
                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse2_29" aria-expanded="false" aria-controls="collapse2">
                                    <h4 class="mbr-fonts-style display-5">
                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>&nbsp;Comment procéder au paiement ?</h4>
                                </a>

                            </div>
                            <div id="collapse2_29" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body p-4">
                                    <p class="mbr-fonts-style text-black panel-text display-7">Après avoir réserver votre commande, un mail de paiement vous sera envoyé dans votre adresse e-mail dans le meilleur délai possible.<br> Vous serez ensuite rédirigé vers la page de paiement après avoir eu le mail. Vous cliquez sur le bouton "<strong>Payer sans compte PayPal</strong>". </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>
                                                <img src="assets/images/office/paypal1.png" class="img-fluid">
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                <img src="assets/images/office/paypal2.png" class="img-fluid">
                                            </p>
                                        </div>
                                    </div>
                                    <p class="text-black">Une fois terminé, votre commande avec les explications vous seront envoyées instantanément par mail.</p>
                                            
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapse3_29" aria-expanded="false" aria-controls="collapse3">
                                    <h4 class="mbr-fonts-style display-5">
                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span>&nbsp;Que faut-t-il faire après avoir eu le produit ?</h4>
                                </a>
                            </div>
                            <div id="collapse3_29" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body p-4">
                                    <p class="mbr-fonts-style text-black panel-text display-7">
                                       Normalement si vous avez votre commande, vous avez un compte de Microsoft office pour vos logiciels (Word, Excel, etc..). Dès la première connexion à votre compte, il est obligatoire de <strong>modifier</strong> votre mot de passe et de <strong>conserver</strong> vos identifiants pour la prochaine connexion. 
                                   </p>
                                   <p class="mbr-fonts-style text-danger panel-text display-7">
                                       <strong>* ATTENTION ! </strong> un compte dont les identifiants sont oubliés, ne sera pas rembourser et il peut dans ce cas là acheter un nouveau produit.
                                   </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-core="" href="#collapseq4_29" aria-expanded="false" aria-controls="collapseq4">
                                    <h4 class="mbr-fonts-style display-5">
                                        <span class="sign mbr-iconfont mbri-arrow-down inactive"></span> Comment gérer son(ses) appareil(s)</h4>
                                </a>
                            </div>
                            <div id="collapseq4_29" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body p-4">
                                    <p class="mbr-fonts-style text-black panel-text display-7">
                                        À partir de votre espace, cliquez sur <strong>Mon compte > Périphérique</strong> et vous y êtes à la gestion de vos appareils.
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="cid-s7xAamyasU mbr-reveal" id="footer1-d">

    

    

    <div class="container">
        <div class="media-container-row content text-white">
            <div class="col-12 col-md-3">
                <div class="media-wrap">
                    <a href="https://mobirise.co/">
                        <img src="assets/images/a6c97cb2-e89e-40f1-91d4-a87cd6a97989-200x200-200x200.png" alt="Mobirise" title="">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">
                    Contacts
                </h5>
                <p class="mbr-text">
                    Email : moussainssa@outlook.fr &nbsp;&nbsp;<br>Tél : +33 (0) 7 68 32 36 99 &nbsp;</p>
            </div>
            <div class="col-12 col-md-3 mbr-fonts-style display-7">
                <h5 class="pb-3">&nbsp;
                </h5>
                <p class="mbr-text">
                    <a href="contact.php" class="btn btn-secondary">M'écrire</a>
                </p>
            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row mbr-white">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Business Software - Tous droits réservés</p>
                </div>
                <div class="col-md-6">
                    <div class="social-list align-right">
                        <div class="soc-item">
                            <a href="https://twitter.com/mobirise" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-snapchat socicon" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                            </a>
                        </div>
                        <div class="soc-item">
                            <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                <span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
                            </a>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


  <script src="assets/web/assets/jquery/jquery.min.js"></script>
  <script src="assets/popper/popper.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/tether/tether.min.js"></script>
  <script src="assets/web/assets/cookies-alert-plugin/cookies-alert-core.js"></script>
  <script src="assets/web/assets/cookies-alert-plugin/cookies-alert-script.js"></script>
  <script src="assets/dropdown/js/nav-dropdown.js"></script>
  <script src="assets/dropdown/js/navbar-dropdown.js"></script>
  <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
  <script src="assets/mbr-switch-arrow/mbr-switch-arrow.js"></script>
  <script src="assets/smoothscroll/smooth-scroll.js"></script>
  <script src="assets/theme/js/script.js"></script>
  
  
<input name="cookieData" type="hidden" data-cookie-customDialogSelector='null' data-cookie-colorText='#424a4d' data-cookie-colorBg='rgba(255, 127, 159, 0.99)' data-cookie-textButton='Ok' data-cookie-colorButton='' data-cookie-colorLink='#424a4d' data-cookie-underlineLink='true' data-cookie-text="En poursuivant votre navigation sur notre site, vous acceptez l'installation et l'utilisation de cookies sur votre poste. Lire notre <a href='privacy.php'>politique de confidentialité</a>.">
  </body>
</html>